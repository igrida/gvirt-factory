# gvirt factory

## Generate images on IGRIDA

### Build image

```bash
oarsub -I -l /cpu=1/core=4,walltime=1 -p "virt='YES'"
cd /srv/tempdd/${USER}
git clone https://gitlab.inria.fr/igrida/gvirt-factory.git
cd gvirt-factory/packer-templates
module load spack/packer/1.4.4
packer build -only qemu -var 'context=igrida' debian-8.7-x86_64.json
```

To add debug messages :

```
node $ PACKER_LOG=1 ~/bin/packer build -only qemu -var 'context=igrida' debian-8.7-x86_64.json
```

Resulting image will be installed into `packer-debian-8.7-x86_64-qemu/` directory.