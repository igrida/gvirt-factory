#!/bin/sh -eux

# Uses edge for recent docker
echo "http://dl-cdn.alpinelinux.org/alpine/v3.12/community/" >> /etc/apk/repositories

apk update
apk add docker
rc-update add docker boot
service docker start

#echo "kernel.pax.softmode=1" > /etc/sysctl.d/01-docker.conf

