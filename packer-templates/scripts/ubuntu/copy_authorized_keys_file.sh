#!/bin/sh

if ! [ -d /root/.ssh ]; then
  ssh-keygen -t rsa -C "gvirt local key" -N "" -f /root/.ssh/id_rsa
fi

cp /mnt/home/.ssh/authorized_keys /root/.ssh
chmod 600 /root/.ssh/authorized_keys
chown root.root /root/.ssh/authorized_keys

if [ -f /mnt/home/.ssh/id_rsa_vm.pub ]; then cat /mnt/home/.ssh/id_rsa_vm.pub >> /root/.ssh/authorized_keys; fi

