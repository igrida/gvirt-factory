#!/bin/sh -eux

if [ -z $context ]; then
  exit 0
elif [ $context != "igrida" ]; then
  exit 0
fi

mkdir -p /mnt/temp_dd/igrida-fs1
mkdir -p /mnt/srv/local
mkdir -p /mnt/home
mkdir -p /mnt/nfs

echo "srv  /mnt/srv  9p  trans=virtio,version=9p2000.L,nofail,x-systemd.device-timeout=10s 0 0" >> /etc/fstab
echo "home  /mnt/home  9p  trans=virtio,version=9p2000.L,nofail,x-systemd.device-timeout=10s 0 0" >> /etc/fstab
echo "nfs  /mnt/nfs  9p  trans=virtio,version=9p2000.L,nofail,x-systemd.device-timeout=10s 0 0" >> /etc/fstab


