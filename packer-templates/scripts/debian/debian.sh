#!/bin/sh -eux

cp /tmp/copy_authorized_keys_file.sh /usr/local/sbin
cp /tmp/copy_authorized_keys_file.service /etc/systemd/system
chown root.root /usr/local/sbin/copy_authorized_keys_file.sh
chmod 755 /usr/local/sbin/copy_authorized_keys_file.sh
chown root.root /etc/systemd/system/copy_authorized_keys_file.service
systemctl enable copy_authorized_keys_file.service

apt-get -y update
echo "pre-up sleep 2" >>/etc/network/interfaces

echo 'APT::Periodic::Enable "0";' >> /etc/apt/apt.conf.d/10periodic

echo '9pnet_virtio' >> /etc/initramfs-tools/modules
update-initramfs -u

apt-get -y purge libx11-data xauth libxmuu1 libxcb1 libx11-6 libxext6;
apt-get -y purge ppp pppconfig pppoeconf;
apt-get -y purge popularity-contest;

apt-get -y autoremove;
apt-get -y clean;

rm -rf /usr/share/doc/*

find /var/cache -type f -exec rm -rf {} \;
find /var/log/ -name *.log -exec rm -f {} \;

