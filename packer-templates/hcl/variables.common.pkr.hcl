variable "preseed_path" {
    type = string
    default = "pressed.cfg"
}

variable "headless" {
    type = bool
    default = true
}

variable "context" {
    type = string
    default = "igrida"

    validation {
        condition = contains(["igrida", "grid5000"], var.context)
        error_message = "Valid values for context are (igrida || grid5000)."
    }
}

locals {
    http_directory = dirname(convert(fileset(".", "etc/http/*"), list(string))[0])
}