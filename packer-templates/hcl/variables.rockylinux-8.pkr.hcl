variable "rockylinux_8_version" {
    default = "8.5"
}

locals {
    #iso_url_rockylinux_8 = "http://download.rockylinux.org/pub/rocky/8.5/isos/x86_64/Rocky-8.5-x86_64-minimal.iso"
    #iso_checksum_rockylinux_8 = "4eb2ae6b06876205f2209e4504110fe4115b37540c21ecfbbc0ebc11084cb779"
    iso_url_rockylinux_8 = "http://download.rockylinux.org/pub/rocky/8.5/isos/x86_64/Rocky-8.5-x86_64-dvd1.iso"
    iso_checksum_rockylinux_8 = "0081f8b969d0cef426530f6d618b962c7a01e71eb12a40581a83241f22dfdc25"
    rockylinux_8_boot_comand = [
        "<up><wait><tab> inst.text inst.ks=http://{{ .HTTPIP }}:{{ .HTTPPort }}/rockylinux/8/ks.cfg<enter><wait>"
    ]
}