build {
    name = "alpine-singularity"
    description = <<EOF
This build creates Alpine images with singularity for Alpine versions :
* 3.12
For the following builders :
* qemu
EOF

    source "source.qemu.base-alpine-amd64" {
        name = "3.12"
        vm_name = "alpine-3.12-singularity.amd64.qcow2"
        iso_url = local.iso_url_alpine_312
        iso_checksum = local.iso_checksum_alpine_312
        output_directory = "qemu_iso_alpine_amd64"
        boot_command = local.alpine_312_boot_command
    }

    provisioner "file" {
        source = "etc/scripts/alpine/copy_authorized_keys_file.start"
        destination = "/etc/local.d/copy_authorized_keys_file.start"
    }

    provisioner "shell" {
        environment_vars = [
            "context=${var.context}"
        ]
        scripts = [
            "etc/scripts/alpine/alpine.sh",
            "etc/scripts/alpine/singularity.sh",
            "etc/scripts/igrida.sh",
            "etc/scripts/grid5000.sh",
            "etc/scripts/minimize.sh"
        ]
    }

}