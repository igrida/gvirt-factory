build {
    name = "debian"
    description = <<EOF
This build creates Debian images for Debian versions :
* 10
* 11
For the following builders :
* qemu
EOF

    source "source.qemu.base-debian-amd64" {
        name = "11"
        vm_name = "debian-11-amd64.qcow2"
        iso_url = local.iso_url_debian_11
        iso_checksum = local.iso_checksum_debian_11
        output_directory = "qemu_iso_debian_amd64"
        boot_command = local.debian_11_boot_command
    }

    source "source.qemu.base-debian-amd64" {
        name = "10"
        vm_name = "debian-10-amd64.qcow2"
        iso_url = local.iso_url_debian_10
        iso_checksum = local.iso_checksum_debian_10
        output_directory = "qemu_iso_debian_amd64"
        boot_command = local.debian_10_boot_command
    }

    source "source.qemu.base-debian-amd64" {
        name = "9"
        vm_name = "debian-9-amd64.qcow2"
        iso_url = local.iso_url_debian_9
        iso_checksum = local.iso_checksum_debian_9
        output_directory = "qemu_iso_debian_amd64"
        boot_command = local.debian_9_boot_command
    }

    provisioner "file" {
        source = "etc/scripts/ubuntu/copy_authorized_keys_file.sh"
        destination = "/tmp/copy_authorized_keys_file.sh"
    }

    provisioner "file" {
        source = "etc/scripts/ubuntu/copy_authorized_keys_file.service"
        destination = "/tmp/copy_authorized_keys_file.service"
    }

    provisioner "shell" {
        environment_vars = [
            "context=${var.context}"
        ]
        execute_command = "echo 'gvirt' | {{.Vars}} sudo -S -E sh -eux '{{.Path}}'"
        scripts = [
            "etc/scripts/sshd.sh",
            "etc/scripts/debian/debian.sh",
            "etc/scripts/igrida.sh",
            "etc/scripts/grid5000.sh",
            "etc/scripts/minimize.sh"
        ]
    }

}