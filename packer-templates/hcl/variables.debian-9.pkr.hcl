variable "debian_9_version" {
    default = "9.13.0"
}

locals {
    iso_url_debian_9 = "https://cdimage.debian.org/cdimage/archive/${var.debian_9_version}/amd64/iso-cd/debian-${var.debian_9_version}-amd64-netinst.iso"
    iso_checksum_debian_9 = "71c7e9eb292acc880f84605b1ca2b997f25737fe0a43fc9586f61d35cd2eb43b"
    debian_9_boot_command = [
        "<esc><wait>",
        "install <wait>",
        " preseed/url=http://{{ .HTTPIP }}:{{ .HTTPPort }}/debian/preseed.cfg <wait>",
        "debian-installer=en_US.UTF-8 <wait>",
        "auto <wait>",
        "locale=en_US.UTF-8 <wait>",
        "kbd-chooser/method=us <wait>",
        "keyboard-configuration/xkb-keymap=us <wait>",
        "netcfg/get_hostname=vm <wait>",
        "netcfg/get_domain=gvirt <wait>",
        "fb=false <wait>",
        "debconf/frontend=noninteractive <wait>",
        "console-setup/ask_detect=false <wait>",
        "console-keymaps-at/keymap=us <wait>",
        "grub-installer/bootdev=/dev/sda <wait>",
        "<enter><wait>"
    ]
}