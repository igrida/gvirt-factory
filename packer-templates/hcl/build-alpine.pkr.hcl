build {
    name = "alpine"
    description = <<EOF
This build creates Alpine images for Alpine versions :
* 3.12
For the following builders :
* qemu
EOF

    source "source.qemu.base-alpine-amd64" {
        name = "3.12"
        vm_name = "alpine-3.12.amd64.qcow2"
        iso_url = local.iso_url_alpine_312
        iso_checksum = local.iso_checksum_alpine_312
        output_directory = "qemu_iso_alpine_amd64"
        boot_command = local.alpine_312_boot_command
    }

}