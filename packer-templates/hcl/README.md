# HCL templates

## Build

Build all images in default context (igrida)

```sh
packer build ./
```

Build all images in grid5000 context

```sh
packer build --var context=grid5000 ./
```

Build only Debian images

```sh
packer build -only='debian.qemu.*' ./
```