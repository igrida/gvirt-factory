#!/bin/sh -eux

if [ -z $context ]; then
  exit 0
elif [ $context != "grid5000" ]; then
  exit 0
fi

mkdir -p /mnt/home
mkdir /mnt/tmp
mkdir /mnt/grid5000

echo "home  /mnt/home  9p  trans=virtio,version=9p2000.L,nofail,x-systemd.device-timeout=10s 0 0" >> /etc/fstab
echo "grid5000  /mnt/grid5000  9p  trans=virtio,version=9p2000.L,nofail,x-systemd.device-timeout=10s 0 0" >> /etc/fstab
echo "srv  /mnt/srv  9p  trans=virtio,version=9p2000.L,nofail,x-systemd.device-timeout=10s 0 0" >> /etc/fstab
echo "tmp  /mnt/tmp  9p  trans=virtio,version=9p2000.L,nofail,x-systemd.device-timeout=10s 0 0" >> /etc/fstab

