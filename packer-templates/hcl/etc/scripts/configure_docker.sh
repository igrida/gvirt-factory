#!/bin/sh -eux

mkdir -p /etc/docker

if [ -z $context ]; then
  exit 0
elif [ $context = "grid5000" ]; then
  cat << EOF > /etc/docker/daemon.json
{
          "registry-mirrors": ["https://docker-mirror.rennes.grid5000.fr"],
          "insecure-registries" : ["harbor.rennes.grid5000.fr","docker-mirror.rennes.grid5000.fr"]
}
EOF
elif [ $context = "igrida" ]; then
    cat << EOF > /etc/docker/daemon.json
{
          "registry-mirrors": ["https://igrida-dockerhub-mirror.irisa.fr"],
          "insecure-registries" : ["igrida-harbor.irisa.fr","igrida-dockerhub-mirror.irisa.fr"]
}
EOF
fi