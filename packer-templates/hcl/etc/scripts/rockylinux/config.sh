#!/bin/sh -eux

cp /tmp/copy_authorized_keys_file.sh /usr/local/sbin
cp /tmp/copy_authorized_keys_file.service /etc/systemd/system
chown root.root /usr/local/sbin/copy_authorized_keys_file.sh
chmod 755 /usr/local/sbin/copy_authorized_keys_file.sh
chown root.root /etc/systemd/system/copy_authorized_keys_file.service
systemctl enable copy_authorized_keys_file.service

# virtio-9p
dnf -y install kernel-modules

# clean
dnf -y remove gcc cpp gc kernel-devel kernel-headers glibc-devel elfutils-libelf-devel glibc-headers kernel-devel kernel-headers
dnf -y autoremove
dnf -y remove linux-firmware

rm -f /etc/udev/rules.d/70-persistent-net.rules;
mkdir -p /etc/udev/rules.d/70-persistent-net.rules;
rm -f /lib/udev/rules.d/75-persistent-net-generator.rules;
rm -rf /dev/.udev/;

for ndev in `ls -1 /etc/sysconfig/network-scripts/ifcfg-*`; do
    if [ "`basename $ndev`" != "ifcfg-lo" ]; then
        sed -i '/^HWADDR/d' "$ndev";
        sed -i '/^UUID/d' "$ndev";
    fi
done

find /var/log -type f -exec truncate --size=0 {} \;
rm -rf /tmp/* /var/tmp/*
rm -f /var/lib/systemd/random-seed
truncate -s 0 /etc/machine-id
rm -f /root/.wget-hsts