#!/bin/sh -eux

apk update && apk upgrade

sed -i '/^PermitRootLogin yes/d' /etc/ssh/sshd_config
sed -i 's/^#PasswordAuthentication yes/PasswordAuthentication no/' /etc/ssh/sshd_config
echo "UseDNS no" >> /etc/ssh/sshd_config
mkdir /root/.ssh
chmod a+x /etc/local.d/copy_authorized_keys_file.start
rc-update add local
