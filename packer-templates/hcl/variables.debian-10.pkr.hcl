variable "debian_10_version" {
    default = "10.11.0"
}

locals {
    iso_url_debian_10 = "https://cdimage.debian.org/cdimage/archive/${var.debian_10_version}/amd64/iso-cd/debian-${var.debian_10_version}-amd64-netinst.iso"
    iso_checksum_debian_10 = "133430141272d8bf96cfb10b6bfd1c945f5a59ea0efc2bcb56d1033c7f2866ea"
    #iso_checksum_debian_10 = "https://cdimage.debian.org/cdimage/release/${var.debian_11_version}/amd64/iso-cd/SHA256SUMS"
    debian_10_boot_command = [
        "<esc><wait>",
        "install <wait>",
        " preseed/url=http://{{ .HTTPIP }}:{{ .HTTPPort }}/debian/preseed.cfg <wait>",
        "debian-installer=en_US.UTF-8 <wait>",
        "auto <wait>",
        "locale=en_US.UTF-8 <wait>",
        "kbd-chooser/method=us <wait>",
        "keyboard-configuration/xkb-keymap=us <wait>",
        "netcfg/get_hostname=vm <wait>",
        "netcfg/get_domain=gvirt <wait>",
        "fb=false <wait>",
        "debconf/frontend=noninteractive <wait>",
        "console-setup/ask_detect=false <wait>",
        "console-keymaps-at/keymap=us <wait>",
        "grub-installer/bootdev=/dev/sda <wait>",
        "<enter><wait>"
    ]
}