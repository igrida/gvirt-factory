variable "alpine_312_version" {
    default = "3.12.9"
}

locals {
    iso_url_alpine_312 = "http://dl-cdn.alpinelinux.org/alpine/v3.12/releases/x86_64/alpine-virt-${var.alpine_312_version}-x86_64.iso"
    #iso_checksum_alpine_312 = "bb87644518e85972582e25b5c872854f28de615e04cb58c1cdaadc46d3ae60ba"
    iso_checksum_alpine_312 = "468d1567afca4787c70d4963a7236d8e65af0c1b261de0efb68b4aec7c971f06"
    alpine_312_boot_command = [
        "root<enter><wait>",
        "ifconfig eth0 up && udhcpc -i eth0<enter><wait5>",
        "wget http://{{ .HTTPIP }}:{{ .HTTPPort }}/alpine/3.12/answerfile-qemu<enter><wait>",
        "setup-alpine -f answerfile-qemu<enter><wait5>",
        "!!packer123<enter><wait>",
        "!!packer123<enter><wait>",
        "<wait10><wait10><wait10>",
        "y<enter>",
        "<wait10>",
        "rc-update --quiet add sshd default<enter><wait>",
        "mount /dev/sda2 /mnt<enter>",
        "echo 'PermitRootLogin yes' >> /mnt/etc/ssh/sshd_config<enter>",
        "umount /mnt<enter>",
        "reboot<enter>"
    ]
}