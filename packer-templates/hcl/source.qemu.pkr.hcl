source "qemu" "base-debian-amd64" {
    headless         = var.headless
    floppy_files     = [
        "${local.http_directory}/debian/preseed.cfg",
    ]
    http_directory   = local.http_directory
    shutdown_command = "echo 'gvirt'|sudo -S shutdown -P now"
    ssh_password     = "gvirt"
    ssh_username     = "gvirt"
    ssh_wait_timeout = "50m"
    disk_size        = 102400
    disk_interface   = "virtio-scsi"
    memory           = 512 * 4
    cpus             = 4
    boot_wait        = "15s"
}

source "qemu" "base-alpine-amd64" {
    headless         = var.headless
    http_directory   = local.http_directory
    shutdown_command = "/sbin/poweroff"
    ssh_password     = "!!packer123"
    ssh_username     = "root"
    ssh_wait_timeout = "50m"
    disk_size        = 102400
    disk_interface   = "virtio-scsi"
    memory           = 512 * 4
    cpus             = 4
    boot_wait        = "15s"
}

source "qemu" "base-rockylinux-amd64" {
    headless         = var.headless
    http_directory   = local.http_directory
    shutdown_command = "echo 'vagrant'|sudo -S /sbin/halt -h -p"
    ssh_password     = "gvirt"
    ssh_username     = "gvirt"
    ssh_wait_timeout = "50m"
    disk_size        = 102400
    disk_interface   = "virtio-scsi"
    memory           = 512 * 4
    cpus             = 4
    boot_wait        = "15s" 
}