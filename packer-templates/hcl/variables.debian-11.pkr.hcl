variable "debian_11_version" {
    default = "11.1.0"
}

locals {
    iso_url_debian_11 = "https://cdimage.debian.org/cdimage/release/${var.debian_11_version}/amd64/iso-cd/${var.debian_11_version}-amd64-netinst.iso"
    iso_checksum_debian_11 = "8488abc1361590ee7a3c9b00ec059b29dfb1da40f8ba4adf293c7a30fa943eb2"
    #iso_checksum_debian_11 = "https://cdimage.debian.org/cdimage/release/${var.debian_11_version}/amd64/iso-cd/SHA256SUMS"
    debian_11_boot_command = [
        "<esc><wait>",
        "install <wait>",
        " preseed/url=http://{{ .HTTPIP }}:{{ .HTTPPort }}/debian/preseed.cfg <wait>",
        "debian-installer=en_US.UTF-8 <wait>",
        "auto <wait>",
        "locale=en_US.UTF-8 <wait>",
        "kbd-chooser/method=us <wait>",
        "keyboard-configuration/xkb-keymap=us <wait>",
        "netcfg/get_hostname=vm <wait>",
        "netcfg/get_domain=gvirt <wait>",
        "fb=false <wait>",
        "debconf/frontend=noninteractive <wait>",
        "console-setup/ask_detect=false <wait>",
        "console-keymaps-at/keymap=us <wait>",
        "grub-installer/bootdev=/dev/sda <wait>",
        "<enter><wait>"
    ]
}